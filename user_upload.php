<?php

// Declare the short options that the user can input CLI
$shortopts = "u:";
$shortopts .= "p:";
$shortopts .= "h:";

// Declare the long options that the user can input via CLI
$longopts  = array(
    "file:",   
    "create_table",
    "dry_run",
    "help",
);

// Assigns the option as the key to the array
$options = getopt($shortopts, $longopts);

// Global variables
// !! Important !! - Change this to the database you wish to connect to
$db_name = "catalystdb";
// --------

$file = "";
$create_table_only = false;
$dry_run = false;

$db_host = "localhost";
$db_user = "";
$db_pass = "";

// If there are options given - start doing relevant processes
if (!empty($options)) {

    $db_args = checkDBArgs($options, $db_host);

    // Displays help text
    if(array_key_exists("help", $options)) {
        displayHelpText();
    }

    // ARG:create_table - Flag to create only or create and insert
    if (array_key_exists("create_table", $options)) {

        // Opens connection to the MySql Database
        $conn = connectToDB($db_args, $db_name);

        try {
            // Attempt to query the table to check if it exists
            // Limit at 1 - ensures no problems if table is extremely large
            $sql = "SELECT ID FROM USERS LIMIT 1";

            // Prep the query and execute
            $stmt = executeSql($sql, $conn);
        
        // Exception is thrown if the table doesnt exist 
        } catch(PDOException $e) {
            print("\nError: " . $e->getMessage()."\n");
            print("\nCreating users table!\n");

            try {
            // Create table
            $sql = createTableSql();

            // Prep the create and execute
            $stmt = executeSql($sql, $conn);

            }  catch(PDOException $e2) { 
                print("\nError: " . $e2->getMessage()."\n");
                print("\Due to above error, exiting script\n");
                $conn = null;
                // Exit with error
                exit(1);
            }
        }
        // If there is no error, it means the sql successfully queried the table - so it already exists
        if (!isset($e)){
            print("\nThe Users table already exists\n");
            // Table exists - Delete then recreate table
            try {
                $sql = deleteTableSql();
                $stmt = executeSql($sql, $conn);
            } catch(PDOException $e3) {
                print("\nError: " . $e3->getMessage()."\n");
                print("\Due to above error, table could not be deleted - exiting script\n");
                $conn = null;
                // Exit with error
                exit(1);
            }

            // Recreate the table here
            try {
                // Create table
                $sql = createTableSql();

                // Prep the create and execute
                $stmt = executeSql($sql, $conn);
    
                }  catch(PDOException $e4) { 
                    print("\nError: " . $e4->getMessage()."\n");
                    print("\nDue to above error, exiting script\n");
                    $conn = null;
                    // Exit with error
                    exit(1);
                }
            }

        // Since no further action is to be taken if the create_table flag is used, exit the script with no error.
        print("\nNo further action to be taken upon use of create_table option. Exiting script.\n");
        $conn = null;
        exit(0);
    }
    
    // ARG:file - Path to users.csv file
    // If there was a file option given, assign given input as the filename to search for
    // Otherwise exit the script and warn user that the filename needs to be declared
    if (array_key_exists("file", $options)) {
        readCSV($options, $db_args, $db_name, $dry_run);
        
    } else {
        print("\nFile not defined, exiting.\n");
        // Exit with error
        exit(1);
    } 

} else {
    displayHelpText();
    // Exit without errors
    exit(0);

}

// Checks that the arguments/options for the database have all been provided
function checkDBArgs($options, $db_host) {
    
    // ARG:u - Database User
    if (array_key_exists("u", $options)) {
        $db_user = $options["u"];
    } else {
        print("\nDatabase user not defined, exiting.\n");
        exit(1);
    }

    // ARG:p - Database Password
    if (array_key_exists("p", $options)) {
        $db_pass = $options["p"];
    } else {
        print("\nDatabase password not defined, exiting.\n");
        exit(1);
    }

    // ARG:h - Database Host
    if (array_key_exists('h', $options)) {
        $db_host = $options["h"];
    } else {
        print("\nHost database not defined, assuming 'localhost'.\n");
    }

    return array('db_user' => $db_user, 'db_pass' => $db_pass, 'db_host' => $db_host);
}

// Database Functions -------------------------------------------- //
// Connects to the MySQL database
function connectToDB($db_args, $db_name){

    $db_user = $db_args['db_user'];
    $db_pass = $db_args['db_pass'];
    $db_host = $db_args['db_host'];

    $conn = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
    // // set the PDO error mode to exception
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    return $conn;
}

// Function to create the mysql table
function createTableSql(){

    $sql = "CREATE TABLE `users` (
        `id` int NOT NULL AUTO_INCREMENT,
        `name` varchar(255) NOT NULL,
        `surname` varchar(255) NOT NULL,
        `email` varchar(255) NOT NULL,
        PRIMARY KEY (`id`),
        UNIQUE KEY `email_UNIQUE` (`email`),
        UNIQUE KEY `id_UNIQUE` (`id`)
      ) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";

      return $sql;

}

// Function to drop the users table
function deleteTableSql(){

   $sql= "DROP TABLE users";

   return $sql;
}

// Runs the SQL given to the function and returns the statement
function executeSql($sql, $conn){

    $stmt = $conn->prepare($sql);
    $stmt->execute();

    return $stmt;
}

function insertIntoDb($row, $db_args, $db_name){
    $conn = connectToDB($db_args, $db_name);

            // Make the names upper case before inserting to DB
            $fname = ucwords($row[0]);
            $sname = ucwords($row[1]);
    
            $email = $row[2];
            
            // Lower and also remove white space from email address
            $email = strtolower(str_replace(' ', '', $email));
            // If email is invalid, print to STDOUT and return
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                print("\nInvalid email format: ");
                print("\n" . $fname . ", " . $sname . ", " . $email);
                return;
            } 
    try{
        // Attempt to query the table to check if it exists
        // Limit at 1 - ensures no problems if table is extremely large
        $sql = "INSERT INTO users (name, surname, email) VALUES (?, ?, ?)";

        // Prep the insert and add the data literals when executing
        $stmt = $conn->prepare($sql);




        $stmt->execute([$fname, $sname, $row[2]]);

    }  catch(PDOException $e) { 
        print("\nError: " . $e->getMessage()."\n");
        if ($e->errorInfo[1] == 1062) {
            // duplicate entry, continue
            print("\nDuplicate Email found - Ignoring and continuing\n");
         } else {
            print("\nDue to above error, exiting script\n");
            $conn = null;
            // Exit with error
            exit(1);
         }

    }

}

// -------------------------------------------------------------- //

// Get the current directory and the csv
function readCSV($options, $db_args, $db_name, $dry_run){
    $dir = getcwd();
    $file = $dir . DIRECTORY_SEPARATOR . $options["file"];
    
    // ARG:dry_run - Dry run switch
    if (array_key_exists('dry_run', $options)) {
        $dry_run = true;
        print("\nYou have Dry Run mode enabled. The data will NOT be entered into the database");
    } 

    // Check that the file has .csv
    if (preg_match("/\.(csv)$/", $options["file"])) {
        // Open the file in read or error if cannot
        $handle = fopen($file, "r") or exit("Unable to open file!");
        // We dont want the first row since they're just headers. Skip the first iteration
        $iteration = 0;
        while (($row = fgetcsv($handle)) !== false) {
            if($iteration != 0){
                print("\nReading CSV - Name: " . $row[0] . " Surname: " . $row[1] . " Email: " .  $row[2]);
                    // If we are doing a dry run, omit the insert to DB
                    if($dry_run != true) {
                        insertIntoDb($row, $db_args, $db_name);
                        $conn = null;
                    }
            }
            $iteration ++;
        }
        // Close the file
        fclose($handle);
        } else {
            print("Please ensure the file is a .csv!");
        }
}

function displayHelpText(){
        // Output information for the user to see if attempting to run the script without any options
        print("\nPlease select one of the following options:\n");
        print("--file [csv file name] – this is the name of the CSV to be parsed\n");
        print("--create_table – this will cause the MySQL users table to be built (and no further action will be taken)\n");
        print("--dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB.\n");
        print("All other functions will be executed, but the database won't be altered\n");
        print("-u – MySQL username\n");
        print("-p – MySQL password\n");
        print("-h – MySQL host\n");
        print("--help – which will output the above list of directives with details.\n");
}
?>