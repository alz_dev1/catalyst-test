# Catalyst Test
# Important 

Please make sure you change $db_name = "catalystdb"; to the database you wish to connect to on your device.

# PLEASE ENSURE PDO EXTENSION IS ENABLED FOR PHP INSIDE PHP.INI

# The application will automatically assume the database is on localhost if no host flag is provided.

# Please note that the foobar test is also included in this repo :)

# Commands cheat sheet:

--file [csv file name] – this is the name of the CSV to be parsed
--create_table – this will cause the MySQL users table to be built (and no further action will be taken)
--dry_run – this will be used with the --file directive in case we want to run the script but not insert into the DB.
All other functions will be executed, but the database won't be altered
-u – MySQL username
-p – MySQL password
-h – MySQL host
--help – which will output the above list of directives with details.
