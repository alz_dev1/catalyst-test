<?php
// init the counter
$counter = 1;
while ($counter <= 100){
    // Use modulus
    if(($counter % 3 == 0) && ($counter % 5 == 0)){
        echo('foobar');
    }
    else if ($counter % 3 == 0) {
        echo('foo');
    } else if ($counter % 5 == 0) {
        echo('bar');
    } else {
        echo($counter);
    }
    
// Increment automatically
$counter++;

// Purely for readability
if($counter != 101){
    echo(', ');
}
}